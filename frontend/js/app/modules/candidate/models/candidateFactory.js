svApp.factory('candidateFactory', ['$http', function($http) {
    var fac = {};

    fac.getCandidates = function(callback) {
        return $http.get('/api/candidate/get-candidates').success(callback);
    };

    fac.getCandidate = function(id, callback) {
        return $http.get('/api/candidate/get-candidate/' + id).success(callback);
    };

    fac.create = function(model, callback) {
        var data = {
            Candidate: model
        };
        return $http.post('/api/candidate/create', data).success(callback);
    };

    fac.update = function(model, callback) {
        var data = {
            Candidate: model
        };
        return $http.post('/api/candidate/update/' + model.id, data).success(callback);
    };

    fac.delete = function(id, callback) {
        return $http.post('/api/candidate/delete/' + id).success(callback);
    };

    return fac;
}]);