svApp.controller('candidateListController', [
    '$http',
    'candidateFactory',
    'notification',
    'arrayHelper',
    function(
        $http,
        candidateFactory,
        notification,
        arrayHelper) {

        var candidate = this;

        candidate.title = 'Candidates';
        candidateFactory.getCandidates(function(responce) {
            candidate.list = responce.candidates;
        });

        candidate.delete = function(id) {
            candidateFactory.delete(id, function(response) {
                if (response.success) {
                    arrayHelper.removeFromArrayByProperty(candidate.list, 'id', id);
                    //notification.show('Deleted successfully', response.success);
                } else {
                    notification.show('Don\'t deleted', response.success);
                }
            })
        }
    }
]);