svApp.controller('candidateUpdateController', [
    '$http',
    '$location',
    '$routeParams',
    'candidateFactory',
    'notification',
    'ACTIONS',
    function(
        $http,
        $location,
        $routeParams,
        candidateFactory,
        notification,
        ACTIONS) {

        var candidate = this;
        candidate.actions = ACTIONS;
        candidate.action = ACTIONS.UPDATE;

        candidateFactory.getCandidate($routeParams.id, function(data) {
            candidate.model = data.candidate;
        });

        candidate.update = function() {
            candidateFactory.update(candidate.model, function(response) {
                if (response.success) {
                    $location.path('/candidates');
                    //notification.show('Updated successfully', response.success);
                } else {
                    notification.show('Don\'t updated', response.success);
                }
            });
        }
    }
]);