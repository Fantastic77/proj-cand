svApp.controller('candidateCreateController', [
    '$http',
    '$location',
    'candidateFactory',
    'notification',
    'ACTIONS',
    function(
        $http,
        $location,
        candidateFactory,
        notification,
        ACTIONS) {

        var candidate = this;
        candidate.actions = ACTIONS;
        candidate.action = ACTIONS.CREATE;
        candidate.isNewRecord = true;

        candidate.create = function() {
            candidateFactory.create(candidate.model, function(response) {
                if (response.success) {
                    $location.path('/candidates');
                    //notification.show('Added successfully', response.success);
                } else {
                    notification.show('Don\'t created', response.success);
                }
            });
        }

    }
]);