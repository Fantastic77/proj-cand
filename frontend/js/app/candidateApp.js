// create supervisor application
var svApp = angular.module('svApp', [
    'ngRoute',
    'ngAnimate'
]);

svApp.constant('ACTIONS', {
   CREATE: 'Create',
   VIEW: 'View',
   UPDATE: 'Update'
});

svApp.config(function($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider

        .when('/', {
            templateUrl : 'views/home/home.html',
            controller  : 'homeController as home'
        })

        .when('/candidates', {
            templateUrl : 'views/candidate/list.html',
            controller  : 'candidateListController as candidate'
        })

        .when('/candidate/create', {
            templateUrl : 'views/candidate/form.html',
            controller  : 'candidateCreateController as candidate'
        })

        .when('/candidate/:id/update', {
            templateUrl : 'views/candidate/form.html',
            controller  : 'candidateUpdateController as candidate'
        })

        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);

    // change 'api/...' to real api path in all api requests
    $httpProvider.interceptors.push(function ($q) {
        return {
            'request': function (config) {
                var apiIndex = config.url.indexOf('api');
                if (apiIndex === 0 || apiIndex === 1) {
                    config.url = config.url.replace('api', 'backend/web');
                }
                return config || $q.when(config);
            }
        }
    });

    // send data as 'form-urlencoded' (by default angular send post requests with content-type: 'application/json'
    // uncomment, if needed

    //$httpProvider.defaults.transformRequest = function(data) {
    //    if (data === undefined) { return data; }
    //    return $.param(data);
    //};
    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
});