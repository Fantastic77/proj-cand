svApp.factory('arrayHelper', function() {
    var arrayHelper = {};

    arrayHelper.removeFromArrayByProperty = function(arr, propertyName, propertyValue) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][propertyName] == propertyValue) {
                arr.splice(i, 1);
            }
        }
    };

    return arrayHelper;
});