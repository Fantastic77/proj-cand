svApp.factory('notification', function() {
    var notification = {};

    notification.show = function(message, success) {
        if (success) {
            alert('Success! ' + message);
        } else {
            alert('Error! ' + message);
        }
    };

    return notification;
});