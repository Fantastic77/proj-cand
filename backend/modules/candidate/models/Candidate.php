<?php

namespace app\modules\candidate\models;

use Yii;

/**
 * This is the model class for table "candidate".
 *
 * @property int $id
 * @property string $name
 * @property string $birthday
 * @property int $experience
 * @property string $comment
 * @property string $created_at
 *
 * @property CandidateFramwork[] $candidateFramworks
 * @property Framwork[] $frameworks
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthday', 'created_at'], 'safe'],
            [['experience'], 'integer'],
            [['Yii1', 'Yii2', 'Laravel', 'Symphony', 'Zend'], 'safe'],
            [['name', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birthday' => 'Birthday',
            'experience' => 'Experience',
            'comment' => 'Comment',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateFramworks()
    {
        return $this->hasMany(CandidateFramework::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrameworks()
    {
        return $this->hasMany(Framework::className(), ['id' => 'framework_id'])->viaTable('candidate_framwork', ['candidate_id' => 'id']);
    }
}
