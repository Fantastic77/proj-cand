<?php

namespace app\modules\candidate\models;

use Yii;

/**
 * This is the model class for table "candidate_framework".
 *
 * @property int $candidate_id
 * @property int $framework_id
 *
 * @property Framework $framework
 * @property Candidate $candidate
 */
class CandidateFramework extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_framework';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['candidate_id', 'framework_id'], 'required'],
            [['candidate_id', 'framework_id'], 'integer'],
            [['candidate_id', 'framework_id'], 'unique', 'targetAttribute' => ['candidate_id', 'framework_id']],
            [['framework_id'], 'exist', 'skipOnError' => true, 'targetClass' => Framework::className(), 'targetAttribute' => ['framework_id' => 'id']],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Candidate::className(), 'targetAttribute' => ['candidate_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'candidate_id' => 'Candidate ID',
            'framework_id' => 'Framework ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFramework()
    {
        return $this->hasOne(Framework::className(), ['id' => 'framework_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_id']);
    }
}
