<?php

namespace app\modules\candidate;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\candidate\controllers';

    public function init()
    {
        parent::init();
    }

}
