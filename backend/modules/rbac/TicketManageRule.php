<?php

namespace app\modules\rbac;
use app\modules\ticket\models\Ticket;
use Yii;
use yii\rbac\Rule;
use yii\rbac\Item;


class TicketManageRule extends Rule
{
    public $name = 'isTicketCreator';
    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (isset(Yii::$app->request->queryParams['id'])) {
            $ticket = Ticket::findOne(Yii::$app->request->queryParams['id']);
            return (isset($ticket) and Yii::$app->user->id == $ticket->author_id) ? true : false;
        }
        return false;
    }
}
?>