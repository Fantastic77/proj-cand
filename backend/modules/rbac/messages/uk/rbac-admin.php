<?php

/**
 * Message translations for \mdmsoft\yii2-admin.
 *
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Assignments' => 'Призначення',
    'Users' => 'Користувачі',
    'User' => 'Користувач',
    'Avaliable' => 'Доступно',
    'Assigned' => 'Призначено',
    'Create' => 'Створити',
    'Update' => 'Оновити',
    'Roles' => 'Ролі',
    'Create Role' => 'Створити роль',
    'Name' => 'Ім\'я',
    'Type' => 'Тип',
    'Description' => 'Опис',
    'Rule Name' => 'Ім\'я правила',
    'Data' => 'Дата',
    'Update Role' => 'Оновити роль',
    'Delete' => 'Видалити',
    'Are you sure to delete this item?' => 'Ви впевнені, що хочете видалити цей елемент?',
    'ID' => 'ID',
    'Parent' => 'Батьківський елемент',
    'Parent Name' => 'Ім\'я батьківсього елемента',
    'Route' => 'Маршрут',
    'Username' => 'Ім\'я користувача',
    'Update Permission' => 'Редагувати дозвіл',
    'Permissions' => 'Дозволи',
    'Permission' => 'Дозвіл',
    'Create Permission' => 'Створити дозвіл',
    'Create Permissions' => 'Створити дозволи',
    'Routes' => 'Шляхи',
    'Create route' => 'Створити шлях',
    'New' => 'Новий',
    'Generate Routes' => 'Згенерувати шляхи',
    'Append' => 'Додати',
    'Create Rule' => 'Створити правило',
    'Rules' => 'Правила',
    'Update Rule' => 'Редагувати правило',
    'Create Menu' => 'Редагувати меню',
    'Menus' => 'Меню',
    'Search' => 'Пошук',
    'Reset' => 'Скидання',
    'Update Menu' => 'Редагувати меню',
    'Order' => 'Порядок',
    'Class Name' => 'Ім\'я класу',
    'Assignment' => 'Призначення',
    'Role' => 'Роль',
    'Rule' => 'Правило',
    'Menu' => 'Меню',
    'Help' => 'Допомога',
    'Application' => 'Додаток',
];
