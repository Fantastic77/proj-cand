<?php

namespace app\modules\user;

use yii\web\AssetBundle;

class BillingCardAssets extends AssetBundle
{
    public $sourcePath = '@app/modules/user/assets/billing-card';

    public $js = [
        'js/billing-card.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\widgets\ActiveFormAsset',
    ];
}