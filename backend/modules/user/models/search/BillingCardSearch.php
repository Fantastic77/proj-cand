<?php

namespace app\modules\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\BillingCard as BillingCardModel;


/**
 * BillingCardSearch represents the model behind the search form about `app\modules\user\models\BillingCard`.
 */
class BillingCardSearch extends BillingCardModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'owner_id', 'currency_id'], 'integer'],
            [['number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BillingCardModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'owner_id' => $this->owner_id,
            'currency_id' => $this->currency_id,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number]);

        return $dataProvider;
    }

    /**
     * @param $id
     * @return array
     */
    public function getEmployeeBillingCards($id)
    {
        $employee = $this->findAll(['owner_id' => $id]);
        $cards = [];

        foreach ($employee as $key => $value) {
            $cards[$key]['id'] = $value->id;
            $cards[$key]['owner_id'] = $value->owner_id;
            $cards[$key]['number'] = $value->number;
            $cards[$key]['currency_id'] = $value->currency_id;
            $cards[$key]['currencyAsString'] = $value->getCurrencyAsString();
            $cards[$key]['cardInfoAsString'] = $value->getCardInfoAsString();
        }

        return $cards;
    }
}
