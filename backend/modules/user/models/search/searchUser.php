<?php

namespace app\modules\user\models\search;

use app\modules\user\models\Rank;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\department\models\Department;

/**
 * search represents the model behind the search form about `app\modules\user\models\User`.
 */
class searchUser extends User
{
    public $employmentStatus;
    public $userOnline;
    public $firstName;
    public $lastName;
    public $middleName;
    public $fullName;
    public $rating;
    public $rank;
    public $rolesAsString;
    public $monthlyRateAsString;
    public $department;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'profile_id'], 'integer'],
            [['email', 'username', 'password', 'auth_key', 'access_token', 'userOnline', 'fullName', 'firstName', 'lastName', 'middleName', 'employmentStatus', 'rating', 'rank', 'rolesAsString', 'monthlyRateAsString', 'department'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $userModule = \Yii::$app->getModule('user');
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();
        $tn_rank = Rank::tableName();
        $tn_department = Department::tableName();

        $query = User::find()
            ->from("$tn_user as u");
            /*->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->leftJoin("$tn_rank as r", 'r.id = p.rank_id')
        ;*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $userModule->userPerPage,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'firstName' => [
                  'asc' => ['first_name' => SORT_ASC],
                  'desc' => ['first_name' => SORT_DESC],
                  'label' => Yii::t('user', 'First name'),
                  'default' => SORT_ASC
                ],
                'lastName' => [
                  'asc' => ['last_name' => SORT_ASC],
                  'desc' => ['last_name' => SORT_DESC],
                  'label' => Yii::t('user', 'Last name'),
                  'default' => SORT_ASC
                ],
                'middleName' => [
                  'asc' => ['middle_name' => SORT_ASC],
                  'desc' => ['middle_name' => SORT_DESC],
                  'label' => Yii::t('user', 'Middle name'),
                  'default' => SORT_ASC
                ],
                'employmentStatus' => [
                  'asc' => ['p.status' => SORT_ASC],
                  'desc' => ['p.status' => SORT_DESC],
                  'label' => Yii::t('user', 'Employment Status'),
                  'default' => SORT_ASC
                ],
                'rating' => [
                    'asc' => ['rating' => SORT_ASC],
                    'desc' => ['rating' => SORT_DESC],
                ],
                'rank' => [
                    'asc' => ['r.name' => SORT_ASC],
                    'desc' => ['r.name' => SORT_DESC],
                ],
                'department' => [
                    'asc' => ['d.name' => SORT_ASC],
                    'desc' => ['d.name' => SORT_DESC],
                ],
            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->leftJoin("$tn_profile as p", 'p.id = u.profile_id');
        $query->leftJoin("$tn_rank as r", 'r.id = p.rank_id');
        $query->leftJoin("$tn_department as d", 'd.id = p.department_id');

        if (!empty($this->firstName)) {
            $query->andWhere('p.first_name LIKE "%' . $this->firstName . '%"');
        }
        if (!empty($this->lastName)) {
            $query->andWhere('p.last_name LIKE "%' . $this->lastName . '%"');
        }
        if (!empty($this->middleName)) {
            $query->andWhere('p.middle_name LIKE "%' . $this->middleName . '%"');
        }

        if (!empty($this->fullName)) {
            $query->andWhere('p.first_name LIKE "%' . $this->fullName . '%" ' .
                'OR p.last_name LIKE "%' . $this->fullName . '%"' .
                'OR p.middle_name LIKE "%' . $this->fullName . '%"'
            );
        }
        
        if (!empty($this->monthlyRateAsString)) {
            if ($c = $this->isComparable($this->monthlyRateAsString)) {
                $query->andWhere([$c['condition'], 'p.monthly_rate', $c['value']]);
            } else {
                $query->andWhere(['p.monthly_rate' => $this->monthlyRateAsString]);
            }
        }

        if (isset($this->rating) && $this->rating !== '') {
            if ($c = $this->isComparable($this->rating)) {
                $query->andWhere([$c['condition'], 'p.rating', $c['value']]);
            } else {
                $query->andWhere(['p.rating' => $this->rating]);
            }
        }

        if (!empty($this->employmentStatus)) {
            $query->andWhere(['p.status' => $this->employmentStatus]);
        }

        if (!empty($this->rank)) {
            $query->andWhere(['p.rank_id' => $this->rank]);
        }

        if (!empty($this->department)) {
            $query->andWhere(['p.department_id' => $this->department]);
        }

        if (!empty($this->rolesAsString)) {
            $roles_type = 1;
            $query->leftJoin('auth_assignment as aa', 'aa.user_id = u.id')
                ->leftJoin('auth_item as ai', 'ai.name = aa.item_name')
                ->andWhere(['ai.name' => $this->rolesAsString])
                ->andWhere(['ai.type' => $roles_type]);
        }
        if (!empty($this->userOnline)) {
            switch($this->userOnline) {
                case 'offline':
                    foreach (Yii::$app->user->onlineUsers as $key => $user) {
                        $query->andWhere(['!=', 'u.id', $key]);
                    }
                    break;
                case 'online':
                    $query->andWhere(['u.id' => array_keys(Yii::$app->user->onlineUsers)]);
                    break;
            }

        }

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
