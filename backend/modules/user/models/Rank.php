<?php
namespace app\modules\user\models;

use yii;
use yii\db\ActiveRecord;
use app\modules\user\models\Profile;

/**
 * This is the model class for table "rank".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Profile[] $profiles
 */
class Rank extends ActiveRecord
{
    public static function tableName()
    {
        return 'rank';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'Id'),
            'name' => Yii::t('user', 'Rank'),
        ];
    }

    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['rank_id' => 'id']);
    }
}