<?php

namespace app\modules\user\models;

use Yii;
use app\modules\payments\models\Currency;
use app\modules\user\models\User;

/**
 * This is the model class for table "billing_card".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $number
 * @property integer $currency_id
 *
 * @property Currency $currency
 * @property User $owner
 */
class BillingCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'currency_id'], 'integer'],
            [['number', 'currency_id'], 'required'],
            [['number'], 'string', 'max' => 255],
            [['number'], 'unique'],
            ['number', 'match', 'pattern' => '/[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}/']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'owner_id' => Yii::t('user', 'Owner'),
            'number' => Yii::t('user', 'Number'),
            'currency_id' => Yii::t('user', 'Currency'),
            'currencyAsString' => Yii::t('user', 'Currency'),
            'cardInfoAsString' => Yii::t('user', 'Card Info'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getCurrencyAsString()
    {
        return $this->currency->name;
    }

    public function getCardInfoAsString(){
        return $this->getCurrencyAsString() . ': ' . $this->number;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
