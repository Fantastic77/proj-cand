<?php
namespace app\modules\user\models;

use yii;
use yii\db\ActiveRecord;
use ReflectionClass;
use yii\helpers\Inflector;
use app\modules\payments\models\Currency;
use app\modules\englishLanguageLevels\models\EnglishLanguageLevel;
use app\modules\department\models\Department;
use app\modules\user\models\BillingCard;
/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $skype
 * @property string $icq
 * @property double $monthly_rate
 * @property integer $monthly_rate_currency_id
 * @property double $hourly_rate
 * @property integer $hourly_rate_currency_id
 * @property double $rating
 * @property integer $rank_id
 *
 * @property Currency $hourlyRateCurrency
 * @property string $monthlyRateAsString
 * @property Currency $monthlyRateCurrency
 * @property EnglishLanguageLevels $englishLanguageLevel_id
 * @property string $hourlyRateAsString
 * @property Rank $rank
 * @property User $user
 */
class Profile extends ActiveRecord
{
    public $profileImage;
    public static $defaultImage = 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4RDkyMjIzOEIyQkExMUU1ODNFNEE4NjFDNUMzNjY2NyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4RDkyMjIzOUIyQkExMUU1ODNFNEE4NjFDNUMzNjY2NyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjhEOTIyMjM2QjJCQTExRTU4M0U0QTg2MUM1QzM2NjY3IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjhEOTIyMjM3QjJCQTExRTU4M0U0QTg2MUM1QzM2NjY3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAgICAgICAgICAgMCAgIDBAMCAgMEBQQEBAQEBQYFBQUFBQUGBgcHCAcHBgkJCgoJCQwMDAwMDAwMDAwMDAwMDAEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwMDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAQABAAwERAAIRAQMRAf/EAHkAAQEBAQEAAAAAAAAAAAAAAAAHBggJAQEBAQEBAAAAAAAAAAAAAAAABAMFAhAAAgEDAgUCBAcAAAAAAAAAAQIDAAQFESFREhMGB4EiMUFhkXHB0UKCkkQRAQACAQQCAgMAAAAAAAAAAAABAgMRIUEEMXFREmEiMv/aAAwDAQACEQMRAD8A9N67qAoFBtsN2DnssiTvGuNtX3WW51DsOKxjf76VNk7VKbeWlcUy0GY8cLi8Rc3lvNc5a9iAIhjVUUD9zcujM2g30B1rOnbm1tJ2h7ti0j8pTVrAoFAoFBUvGeCtb65u8rdxiYY9kjtI2GqiRhzFyOKjTSo+5kmIisctsNdd5XOucpKCF+TMHbWN1aZW0jEIyDPHdxqNFMijmD6cWGuv4V0epkmYms8Js1dN4S6rGJQKBQW/xS4ONy0fzW6Vj/KMD8q53c/qPSnB4lVqjbFBKfKzgY7Ex/Nrpm/rGf1q3pf1Pphn8QiFdBOUCgUF58WvC2DvEVFWeO7YTOB7mBVSup+m4Fc3uRP3j0pw+FMqRsUEz8pPCuDs0dFaeS7UQuR7lAVi2h+uwNV9OP3n0xzeEGrpJigUCg3/AI4ykePzxt5nCRZOLoAk6DqqeZPvuPWpe1T7U1+GmK2lnQ1cxWUHPPkfKR5DPC3hcPFjIuixB1HVY8z/AG2HpXT6lPrTX5S5Z1lgKqZFAoFBoe1LCfI9wYuGBOfozx3E+4HLHEwZm3rLPaK0mZe6RrLqSuOsKDlvuuwnx3cGUhnTk608k8G4PNHKxZW2rsYLRakaI7xpLPVq8FAoNTiOzc/mSrQWZtrZv9dzrGmnEAjmb0FY5OxSnOr3XHNln7V7Lte2mkujcteX80fTklICoq6gkIu53I+JNc/N2JybcKKY4q2tYNCgxXdXZdr3K0d0LlrO/hj6ccoAZGXUkB12PxPxBqjD2Jx7cM744si+X7Oz+G5nnszc2y/67bWRNOJAHMvqKvx9il+dE9sc1Zetnh0zhOzMJhFRo7Zbu8Ue6+nAZ9eKg7L6VyMme9+dllccVaysXsoFAoFAoMnnOzMJm1d5LZbS8Ye2+gAV9eLAbN61tjz2pzs8WpFn/9k=';

//    public $billingUSD;
//    public $billingUAH;

    const STATUS_PROFILE_FREE = 1;
    const STATUS_PROFILE_PROJECT = 2;
    const STATUS_PROFILE_ESSAY = 3;

    const SCENARIO_STATUS = 'status';

    public static function tableName()
    {
        return 'profile';
    }

    public function rules()
    {
        return [
            [['monthly_rate', 'hourly_rate', 'rating'], 'number'],
            [['monthly_rate_currency_id', 'hourly_rate_currency_id', 'rank_id', 'englishLanguageLevel_id', 'department_id'], 'integer'],
            [['first_name', 'last_name', 'middle_name', 'phone', 'skype', 'icq', 'profile_image', /*'billing_info',*/ 'email_home'], 'string', 'max' => 255],
            ['rating', 'double', 'min' => 0, 'max' => 10],
            [['profileImage'], 'file', 'skipOnEmpty' => true],
//            ['billingUSD', 'string'],
//            ['billingUAH', 'string'],
            ['status', 'integer'],
            ['status_description', 'string', 'max' => 30],
            [['status', 'status_description'], 'required', 'on' => self::SCENARIO_STATUS],
            ['birthday', 'safe'],
            ['email_home', 'email'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_STATUS] = ['status', 'status_description'];
        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'Id'),
            'first_name' => Yii::t('user', 'First name'),
            'last_name' => Yii::t('user', 'Last name'),
            'middle_name' => Yii::t('user', 'Middle name'),
            'phone' => Yii::t('user', 'Phone'),
            'skype' => Yii::t('user', 'Skype'),
            'icq' => Yii::t('user', 'Icq'),
            'monthly_rate' => Yii::t('user', 'Monthly rate'),
            'monthly_rate_currency_id' => Yii::t('user', 'Monthly rate currency'),
            'hourly_rate' => Yii::t('user', 'Hourly rate'),
            'hourly_rate_currency_id' => Yii::t('user', 'Hourly rate currency'),
            'rating' => Yii::t('user', 'Rating'),
            'rank_id' => Yii::t('user', 'Rank'),
            'monthlyRateAsString' => Yii::t('user', 'Monthly rate'),
            'hourlyRateAsString' => Yii::t('user', 'Hourly rate'),
            'englishLanguageLevel_id' => Yii::t('user', 'English language level'),
            'profile_image' => Yii::t('user', 'Profile Image'),
            'profileImage' => Yii::t('user', 'Profile Image'),
//            'billingUSD' => Yii::t('user', 'Billing USD'),
//            'billingUAH' => Yii::t('user', 'Billing UAH'),
//            'billing_info' => Yii::t('user', 'Billing information'),
            'status' => Yii::t('app', 'Employment status'),
            'status_description' => Yii::t('app', 'Employment status description'),
            'department_id' => Yii::t('user', 'Department'),
            'email_home' => Yii::t('user', 'Home email'),
            'birthday' => Yii::t('user', 'Birthday'),
        ];
    }

    public static function statusDropdown() {
        $dropdown = [];
        $constPrefix = "STATUS_PROFILE_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_ACTIVE)
        $i = 0;
        foreach ($constants as $constantName => $constantValue) {
            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $dropdown[$i]['id'] = $constantValue;
                $dropdown[$i]['label'] = Yii::t('user', $prettyName);
                $i++;
            }
        }
        return $dropdown;
    }

    public static function getStatusAsString($statusID)
    {
        $statuses = self::statusDropdown();
        $status = array_search($statusID, array_column($statuses, 'id'));
        return $statuses[$status]['label'];
    }

    public function findStatus($v) {
        if (strlen($v) == 0)
            return 0;
        if ( stristr( strtolower(Yii::t('user', 'Free')), strtolower($v) ) )
            return Profile::STATUS_PROFILE_FREE;
        else if ( stristr( strtolower(Yii::t('user', 'Project')), strtolower($v) ) )
            return Profile::STATUS_PROFILE_PROJECT;
        else if ( stristr( strtolower(Yii::t('user', 'Essay')), strtolower($v) ) )
            return Profile::STATUS_PROFILE_ESSAY;
        return 0;
    }

    public function getRank() {
        return $this->hasOne(Rank::className(), ['id' => 'rank_id']);
    }

    public function getMonthlyRateCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'monthly_rate_currency_id']);
    }

    public function getMonthlyRateAsString() {
        return $this->monthly_rate . ' ' . (($this->monthlyRateCurrency === null) ? null : $this->monthlyRateCurrency->name);
    }

    public function getHourlyRateCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'hourly_rate_currency_id']);
    }

    public function getHourlyRateAsString() {
        return $this->hourly_rate . ' ' . (($this->hourlyRateCurrency === null) ? null : $this->hourlyRateCurrency->name);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['profile_id' => 'id']);
    }

    public function getEnglishLanguageLevel(){
        return $this->hasOne(EnglishLanguageLevel::className(), ['id' => 'englishLanguageLevel_id']);
    }

    public function getBillingCards(){
        return $this->hasMany(BillingCard::className(), ['id' => 'owner_id']);
    }
    
    public function getProfileImage() {
        return ($this->profile_image === null) ? self::$defaultImage : Yii::$app->user->avatarsDir . $this->profile_image;
    }

    // return image with url or default image
    public static function formatProfileImage($image) {
        return ($image === null) ? self::$defaultImage : Yii::$app->user->avatarsDir . $image;
    }

    public function getDepartment(){
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }
}