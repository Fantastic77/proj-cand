$(document).on('click', 'body', function(e){
  if (!$(e.target).closest('.user-more-info').length) {
    $('#user_more_info_container').animate({right:-$('#user_more_info_container').outerWidth(true)}, 500);
  }
});

$(document).on('click', '.user-more-info', function(e){
  var id = $(this).parent().attr('data-key');
  $.ajax({
    url: "/employee/info/" + id,
    method: 'get',
    success: function (userData) {
      var rank = '';
      if (userData.rank.value != null) {
        rank = "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.rank.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.rank.value + "." + "</div>" +
          "</div>";
      }
      $('#user_more_info_container').html('<div style="position: absolute; top: 50%; left: 50%;"><img src="/images/loading.svg"></div>');
      setTimeout(function() {
        $('#user_more_info_container').html(
          "<span class='glyphicon glyphicon-arrow-right close' aria-hidden='true'></span>" +
          "<div class='profile-image text-center'>" + "<img class='user-more-info-image image-responsive' src='" + userData.profile_image.value + "' width=125 height=125></div>" +
          "<div class='user-status text-center'>" + userData.online.value  + "</div>" +
          "<div class='user-rating text-center'>" + "<input id='input-id' value='" + userData.rating.value +"'>" + "</div>" +
          "<div class='container-fluid'>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.first_name.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.first_name.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.last_name.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.last_name.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.middle_name.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.middle_name.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.roles.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.roles.value + "." + "</div>" +
          "</div>" + rank +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.profile_status.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.profile_status.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.profile_status_description.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.profile_status_description.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.department.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.department.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.skype.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.skype.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.icq.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.icq.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.phone.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.phone.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.email.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.email.value + "." + "</div>" +
          "</div>" +

          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.email_home.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.email_home.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.birthday.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.birthday.value + "." + "</div>" +
          "</div>" +

          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.updated.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.updated.value + "." + "</div>" +
          "</div>" +  "<br>" +
          "<div class='row'>" +
          "<div class='col-xs-10 col-xs-offset-1'>" +
          '<a href="javascript:void(0)" class="col-xs-12 btn btn-info send-message"   data-target="#create-dialog-modal" data-toggle="modal" data-user-id="' + id +  '"  >' + userData.message.label + '</a>' +
          "</div>" +
          "</div>" +
          "</div>"
        );
        $('#input-id').rating({min: 0, max: 10, stars: 10, step: 0.5, size: 'xs', showClear: false, readonly: true, showCaption: false});
      }, 1000);
      if (parseInt($('#user_more_info_container').css("right")) < -100) {
        $('#user_more_info_container').animate({right: -$('#user_more_info_container').outerWidth(true)}, 500).animate({right: 0}, 500);
      }
    },
    error: function() {
    },
    complete: function() {
    }
  })

});

$(document).on('click', '#user_more_info_container span.close', function(e){
  $('#user_more_info_container').animate({right:-$('#user_more_info_container').outerWidth(true)}, 500);
});

$(document).on('click', '.send-message', function() {
  var userId = $(this).data('user-id');
  $('#newDialogUsersIds').val(userId);
  $('#newDialogUsersIds').trigger('change');
});