$(function(){
    $('#modalButtonAddBillingCard').click(function(){
        $('#modalAddBillingCard').modal('show')
            .find('#modalContentAddBillingCard')
            .load($(this).attr('value'));
    });
});

$(document).on('beforeSubmit', '#billingCardForm', function(event) {
    var form = $(this);

    $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: form.serialize(),
        success: function (data) {
            if(Boolean(data.save) === true) {
                data.billingCardModel.currencyAsString = data.currencyAsString;

                var count = $('#billing-cards ul').children('li').size();
                if(count >= 0){
                    $('#billing-cards ul span').first().html(data.text);
                }

                var template = getBillingCardTemplate(data.billingCardModel);
                $('#billing-cards ul').append(template);
                $('#modalAddBillingCard').modal('hide');
            }
        },
        error: function () {
            alert('Error');
        },
    });

    return false;
});

function getBillingCardTemplate(billingCardModel) {
    var cardId = (billingCardModel.id != null) ? billingCardModel.id : null;

    var deleteButton =
        '<span style="float: right;"  class="glyphicon glyphicon-remove float-right"></span>';
    var hiddenInput =
        '<input type="hidden" name="card[]" value="number=' + billingCardModel.number + ';currency_id=' + billingCardModel.currency_id + ';id=' + cardId + ' "></input>';

    var result =
        '<li class="list-group-item">' +
        '<a class="deleteCard" href="/user/billing-card/delete">' +
        deleteButton +
        '</a>' +
        hiddenInput +
        billingCardModel.currencyAsString + ': ' + billingCardModel.number +
        '</li>';

    return result;
}

$(document).on('click', '.deleteCard', function(event) {
    var link = $(this);
    var card = $(this).parent();
    $.ajax({
        url: link.attr('href'),
        method: 'get',
        success: function(data) {
            if (Boolean(data.save) === true) {
                card.remove();

                var count = $('#billing-cards ul').children('li').size();
                if(count == 0){
                    $('#billing-cards ul span').first().html(data.text);
                }
            }
        },
        error: function() {
            alert('Error');
        },
        complete: function() {

        }
    });

    return false;
});


