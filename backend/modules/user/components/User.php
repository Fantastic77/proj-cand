<?php

namespace app\modules\user\components;

use app\modules\user\models\Profile;
use Yii;
use yii\base\Exception;
use yii\base\ExitException;
use yii\redis\Connection;

class User extends \yii\web\User
{

    public $identityClass = 'app\modules\user\models\User';
    public $enableAutoLogin = true;
    public $redis;
    public $avatarsDir = '/uploads/avatars/';

    public function getDisplayName()
    {
        $user = $this->getIdentity();
        return $user->getDisplayName();
    }

    public function getProfileImage()
    {
        $user = $this->getIdentity();
        $profile = Profile::findOne(['id' => $user['profile_id']]);
        return !empty($profile->profile_image) ? Yii::$app->urlManager->baseUrl . $this->avatarsDir . $profile->profile_image : Profile::$defaultImage;
    }

    public function getProfileStatus()
    {
        $user = $this->getIdentity();
        return Profile::getStatusAsString($user->profile->status);
    }

    public function getOnlineUsers()
    {
        $this->redis = Yii::$app->redis;
        if (!Yii::$app->user->isGuest) {
            try {
                $this->redis->executeCommand('SETEX', [$this->getIdentity()->getId(), Yii::$app->params['online.User'], $this->getDisplayName()]);
            } catch (Exception $e) {
               return ['error' => 'No connection to Redis Server'];
            }
        }
        $keys = $this->redis->executeCommand("keys", ['*']);
        $usersOnline = [];
        if (!empty($keys)) {
            foreach ($keys as $key) {
                if (ctype_digit($key)) {
                    $usersOnline[$key] = $this->redis->get($key);
                }
            }
        }
        return !empty($usersOnline) ? $usersOnline : Yii::t('user', 'No users');
    }

    public function getOnlineUsersCount()
    {
        $onlineUsers = $this->getOnlineUsers();
        if (isset($onlineUsers['error']))
            return 0;
        else return count($onlineUsers) - 1;
    }

    public function getOnlineUsersItems()
    {
        $onlineUsers = $this->getOnlineUsers();
        $items = [];
        $i = 0;
        if (count($onlineUsers) == 1) {
            $items[0]['label'] = Yii::t('app', 'No users online');
        } else {
            foreach ($onlineUsers as $key => $user) {
                if ($key != $this->getIdentity()->getId()) {
                    $items[$i]['url'] = '/user/view?id=' . $key;
                    $items[$i]['label'] = $user;
                    $i++;
                }
            }
        }
        return $items;
    }

    public function getDeleteUserAfterLogout()
    {
        $onlineUsers = $this->getOnlineUsers();
        if (isset($onlineUsers[$this->getIdentity()->getId()])) {
            $this->redis->executeCommand("DEL", [$this->getIdentity()->getId()]);
        }
    }

    public function isUserOnline($id) {
        return array_key_exists($id, $this->getOnlineUsers());
    }

    public function getOnline()
    {
        $usersOnline = $this->getOnlineUsers();
        return isset($usersOnline[Yii::$app->user->id]);
    }

}
