<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\searchRank */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $actionButtonsTemplate String */

$this->title = Yii::t('user', 'Ranks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rank-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php if (Yii::$app->user->can('rank.create')) { ?>
        <p>
            <?= Html::a(Yii::t('user', 'Create Rank'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $actionButtonsTemplate,
            ],
        ],
    ]); ?>

</div>
