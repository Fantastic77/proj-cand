<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\searchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $actionButtonsTemplate String */
/* @var $ranks[] app\modules\user\models\Rank */
/* @var $otherUsers[] app\modules\user\models\User */

$this->title = Yii::t('user', 'Employees');
$this->params['breadcrumbs'][] = $this->title;

kartik\rating\StarRatingAsset::register($this);
app\modules\user\UserAssets::register($this);
?>
<div class="user-index">

    <?= $this->render('@app/modules/dialog/views/dialog/_dialogCreateModal', [
        'otherUsers' => $otherUsers
    ]) ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin();?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'userOnline',
                    'label' => Yii::t('user', 'Status'),
                    'filter' => ArrayHelper::map([0 => ['id' => 'online', 'name' => Yii::t('app', 'Online')], 1 => ['id' => 'offline', 'name' => Yii::t('app', 'Offline')]], 'id', 'name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'firstName',
                    'label' => Yii::t('user', 'First name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'lastName',
                    'label' => Yii::t('user', 'Last name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'middleName',
                    'label' => Yii::t('user', 'Middle name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'rank',
                    'filter' => ArrayHelper::map($ranks, 'id', 'name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'label' => Yii::t('user', 'Department'),
                    'attribute' => 'department',
                    'filter' => ArrayHelper::map($departments, 'id', 'name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'rolesAsString',
                    'filter' => ArrayHelper::map($roles, 'name', 'name'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'employmentStatus',
                    'label' => Yii::t('app', 'Employment status'),
                    'filter' => ArrayHelper::map($profileStatuses, 'id', 'label'),
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'attribute' => 'rating',
                    'contentOptions' => ['class' => 'user-more-info'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $actionButtonsTemplate,
                    'buttons' => [
                        'message' => function ($url, $model) {
                            return '<a href="javascript:void(0)" title = ' . Yii::t("user", "Write") . ' ><span class="glyphicon glyphicon-envelope send-message" data-target="#create-dialog-modal" data-toggle="modal" data-user-id="' . $model->id .'"></span></a>';
                        },
                        'complain' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-exclamation-sign"></span>', "/complaint/create/$model->id",
                                [
                                    'title' => Yii::t('user', 'Complain'),
                                ]);
                        },
                    ],
                ],
            ]
        ]); ?>
    <?php Pjax::end();?>
</div>

<div id="user_more_info_container">
</div>
