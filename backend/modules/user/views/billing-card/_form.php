<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


use kartik\rating\StarRating;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\modules\user\BillingCardAssets;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\BillingCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="billing-card-form">

    <?php $form = ActiveForm::begin(['id' => 'billingCardForm']); ?>

    <?= $form->field($model, 'number')->widget('yii\widgets\MaskedInput', [
        'mask' => '9999-9999-9999-9999'
    ]); ?>

    <?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map($currencies, 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>