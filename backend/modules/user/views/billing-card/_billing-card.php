<?php

use yii\bootstrap\Html;
app\modules\user\BillingCardAssets::register($this);

/**
 * Show card connect
 */
?>


<?php if (!empty($model)): ?>
    <li class="list-group-item">
        <?= Html::a('<span class="glyphicon glyphicon-remove float-right"></span>', ['/user/billing-card/delete', 'id' => $model['id']],
            [
                'class' => 'deleteCard',
            ])
        ?>
        <?= $model['currencyAsString'] . ': ' . $model['number'] ?>
    </li>
<?php endif;?>

