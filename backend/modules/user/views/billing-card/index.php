<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\BillingCard */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user', 'Billing Cards');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-card-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('user', 'Create Billing Card'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'owner_id',
            'number',
            'currency_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
