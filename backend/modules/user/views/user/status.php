<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\ProjectSelect */

$this->title = Yii::t('user', 'Change status');
?>
<div class="user-status-edit">
  <?= $this->render('_form_status', [
    'profile' => $profile,
  ]) ?>
</div>
