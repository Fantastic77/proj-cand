<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \app\modules\user\models\Profile;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\ProjectDeveloper */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-change-status-form">

  <?php $form = ActiveForm::begin(['id' => 'userStatusForm']); ?>
  <?= $form->field($profile, 'status')->dropDownList( ArrayHelper::map(Profile::statusDropdown(), 'id', 'label') ); ?>
  <?= $form->field($profile, 'status_description')->textarea(['rows' => 3, 'maxlength' => true]); ?>

  <div class="form-group">
    <?= Html::submitButton(Yii::t('user', 'Change'), ['class' => 'col-xs-12 btn btn-success']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
