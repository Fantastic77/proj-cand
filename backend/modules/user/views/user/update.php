<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $ranks[] app\modules\user\models\Rank */
/* @var $roles[] use yii\rbac\Role; */
/* @var $currencies[] app\modules\currency\models\Currency */
/* @var $technologies[] app\modules\technology\models\Technology */
/* @var $statuses[] string */

$this->title = Yii::t('user', 'Update User') . ': ' . $userModel->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['list']];
$this->params['breadcrumbs'][] = ['label' => $userModel->fullName, 'url' => ['view', 'id' => $userModel->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('user.update') && !Yii::$app->user->can('profile.View')): ?>
        <?= $this->render('_form', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'ranks' => $ranks,
            'roles' => $roles,
            'currencies' => $currencies,
            'technologies' => $technologies,
            'engLangLvls' => $engLangLvls,
            'statuses' => $statuses,
            'billingCards' => $billingCards,
            'departments' => $departments,
        ]) ?>
    <?php else: ?>
        <?= $this->render('_form_profile', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'technologies' => $technologies,
        ]) ?>
    <?php endif; ?>

</div>
