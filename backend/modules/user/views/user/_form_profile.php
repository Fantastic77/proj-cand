<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\form\ActiveForm;


/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $form yii\widgets\ActiveForm */
/* @var $technologies[] app\modules\technology\models\Technology */
/* @var $action string */

?>

<div class="user-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

  <div class="row">
    <!-- left section -->
    <div class="col-xs-12 col-sm-6">

      <?= $form->field($profileModel, 'first_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'last_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'middle_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($userModel, 'username')->textInput(['maxlength' => true]) ?>

      <?php
      $passwordOptions = ['maxlength' => true, 'value' => ''];
      if ($userModel->isNewRecord) {
        $passwordOptions['required'] = 'required';
      }
      ?>
      <!-- <?= $form->field($userModel, 'password')->passwordInput($passwordOptions) ?>-->

    </div>

    <!-- right section -->
    <div class="col-xs-12 col-sm-6">

      <?= $form->field($userModel, 'email')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'phone')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'skype')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'icq')->textInput(['maxlength' => true]) ?>

    </div>
  </div>

  <hr/>
  <div class="row">

    <div class="col-xs-12 col-sm-3">
      <?= $form->field($userModel, 'technologiesIds')->widget(Select2::className(), [
        'name' => 'technologiesIds',
        'data' => ArrayHelper::map($technologies, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => Yii::t('user', '--select technologie(s)--'), 'multiple' => true],
        'pluginOptions' => ['allowClear' => true],
      ]);?>
    </div>

  </div>
  <hr/>

  <div class="row">
    <div class="col-xs-12 col-sm-6">

<!--        --><?php //= $form->field($profileModel, 'billingUSD', [
//          'feedbackIcon' => [
//            'prefix' => 'fa fa-',
//            'default' => 'phone',
//            'success' => 'check-circle',
//            'error' => 'exclamation-circle',
//          ]
//        ])->widget('yii\widgets\MaskedInput', [
//          'mask' => '9999-9999-9999-9999'
//        ]); ?>

    </div>
    <div class="col-xs-12 col-sm-6">

<!--        --><?php //= $form->field($profileModel, 'billingUAH', [
//          'feedbackIcon' => [
//            'prefix' => 'fa fa-',
//            'default' => 'phone',
//            'success' => 'check-circle',
//            'error' => 'exclamation-circle',
//          ]
//        ])->widget('yii\widgets\MaskedInput', [
//          'mask' => '9999-9999-9999-9999'
//        ]); ?>

    </div>
  </div>
  <hr/>

  <?php if (!empty($profileModel->profile_image)) {
    $prev = Html::img(Yii::$app->urlManager->baseUrl . '/uploads/avatars/' . $profileModel->profile_image, [
      'class' => 'file-preview-image',
      'alt' => Yii::t('user', 'Avatar for ') . $profileModel->first_name . '.',
    ]);
    $url = Url::to(['/user/deleteavatar', 'id' => $profileModel->id]);
    $initialPreviewConfig[] = ['url' => $url];
  } else {
    $prev = null;
    $initialPreviewConfig = [];
  } ?>
  <?= $form->field($profileModel, 'profileImage')->widget(FileInput::className(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
      'showUpload' => false,
      'showRemove' => false,
      'initialPreview' => [
        $prev,
      ],
      'initialPreviewConfig' => $initialPreviewConfig,
    ],
    'pluginEvents' => [
      'filepredelete' => "function(event, key) {
                  return (!confirm('Are you sure you want to delete user avatar?'));
              }",
      'filedeleteerror' => "function(event, data) {
                  $('div.field-profile-profileimage').find('div.file-preview-thumbnails').remove();
              }",
    ],
  ]); ?>
  <hr/>

  <div class="form-group text-center">
    <?= Html::submitButton($userModel->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $userModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
