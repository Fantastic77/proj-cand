<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;
use app\modules\complaint\models\Complaint;
use app\modules\user\models\Profile;

/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */

$this->title = $userModel->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $userModel->fullName;


?>
<div class="user-view">

    <h1><?= Html::encode($userModel->fullName) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('user.update') || Yii::$app->user->can('user.profileView')) { ?>
            <?= Html::a(Yii::t('user', 'Update'), ['update', 'id' => $userModel->id], ['class' => 'btn btn-primary']) ?>
        <?php } ?>

        <?php if (Yii::$app->user->can('user.delete')) { ?>
            <?= Html::a(Yii::t('user', 'Delete'), ['delete', 'id' => $userModel->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>
    <?php $profileImage = !empty($userModel->profile->profile_image) ? Yii::$app->urlManager->baseUrl . '/uploads/avatars/' . $userModel->profile->profile_image : Profile::$defaultImage; ?>    <?= DetailView::widget([
        'model' => $userModel,
        'attributes' => [
            [
                'label' => Yii::t('user', 'Profile image'),
                'format' => 'raw',
                'type' => 'html',
                'value' => Html::img($profileImage, ['width' => 75]),
            ],
            'profile.first_name',
            'profile.last_name',
            'profile.middle_name',
            'username',
            'statusAsString',
            'rolesAsString',
            'profile.rank.name',
            [
                'label' => Yii::t('user', 'Department'),
                'attribute' => 'profile.department.name',
            ],
            'technologiesAsString',
            'profile.englishLanguageLevel.name',
            'email:email',
            'profile.email_home:email',
            'profile.phone',
            'profile.skype',
            'profile.icq',
            [
                'format' => 'raw',
                'label' => Yii::t('user', 'Rating'),
                'type' => 'html',

                'value' => StarRating::widget([
                  'name' => 'rating',
                  'value' => $userModel->getRating(),
                  'pluginOptions' => [
                    'size' => 'xs',
                    'max' => 10,
                    'stars' => 10,
                    'readonly' => true,
                    'showClear' => false,
                    'showCaption' => true,
                  ]
                ]),
            ],
            'profile.monthlyRateAsString',
            'profile.hourlyRateAsString',
//            [
//                'label' => Yii::t('user', 'Billing information'),
//                'value' => $userModel->getBilling(),
//            ],
            'profile.birthday',
        ],
    ]) ?>

    <?php if (count($userModel->complaints) > 0) { ?>
        <hr/>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">
                        <a data-toggle="collapse" href="#collapse1"><strong><?= Yii::t('user', 'Complaints to user') ?></strong></a>
                    </h1>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body">

                        <?php foreach ($userModel->complaints as $complaint) { ?>

                            <?php $type = ($complaint->status == Complaint::STATUS_NEW) ? 'danger' : 'info'; ?>

                            <div class="bs-callout bs-callout-<?= $type; ?>">
                                <h4><?= $complaint->whoFullName ?></h4>
                                <?= $complaint->reason; ?>
                            </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <hr/>
    <?php } ?>

</div>
