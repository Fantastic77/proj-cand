<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\modules\user\BillingCardAssets;
use app\modules\payments\models\Currency;

BillingCardAssets::register($this);

?>

<!--<div id="project-developer">-->
<!--    <ul class="list-group">-->
<!--            --><?php //if (!empty($billingCards)): ?>
<!--                <span>--><?//= Yii::t('user', 'List of cards:') ?><!--</span>-->
<!--            --><?php //else: ?>
<!--                <span>--><?//= Yii::t('user', 'There is no cards added') ?><!--</span>-->
<!--            --><?php //endif; ?>
<!--        </ul>-->
<!--    </div>-->
<!---->
<?php //= Html::button(Yii::t('user', 'Add Billing Card'), ['value' => '/user/billing-card/create', 'class' => 'btn btn-success', 'id'=>'modalButtonAddDeveloper']) ?>
<?php
//Modal::begin([
//    'id' => 'modalAddDeveloper',
//]);
//echo "<div id='modalContentAddDeveloper'></div>";
//Modal::end();
//?>



<!--<div id="project-developer">-->
<!--    <ul class="list-group">-->
<!--        --><?php //if (!empty($billingCards)): ?>
<!--            <span>--><?//= Yii::t('user', 'List of cards:') ?><!--</span>-->
<!--        --><?php //else: ?>
<!--            <span>--><?//= Yii::t('user', 'There is no cards added') ?><!--</span>-->
<!--        --><?php //endif; ?>
<!--    </ul>-->
<!--</div>-->
<!---->
<?php //= Html::button(Yii::t('user', 'Add Billing Card'), ['value' => '/user/billing-card/create', 'class' => 'btn btn-success', 'id'=>'modalButtonAddBillingCard']) ?>
<?php
//Modal::begin([
//    'id' => 'modalAddBillingCard',
//]);
//echo "<div id='modalContentAddBillingCard'></div>";
//Modal::end();
//?>


<div id="billing-cards">
    <ul class="list-group">
        <?php if (!empty($billingCards)): ?>
            <span><?= Yii::t('user', 'List of cards:') ?></span>
        <?php else: ?>
            <span><?= Yii::t('user', 'There is no cards added') ?></span>
        <?php endif; ?>
    </ul>
</div>
<?= Html::button(Yii::t('user', 'Add Billing Card'), ['value' => Url::to(['billing-card/create']), 'class' => 'btn btn-success', 'id'=>'modalButtonAddBillingCard']) ?>
<?php
Modal::begin([
    'id' => 'modalAddBillingCard',
]);
echo "<div id='modalContentAddBillingCard'></div>";
Modal::end();
?>