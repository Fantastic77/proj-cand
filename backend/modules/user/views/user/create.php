<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $ranks[] app\modules\user\models\Rank */
/* @var $roles[] use yii\rbac\Role; */
/* @var $currencies[] app\modules\currency\models\Currency */
/* @var $technologies[] app\modules\technology\models\Technology */
/* @var $statuses[] string */

$this->title = Yii::t('user', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'userModel' => $userModel,
        'profileModel' => $profileModel,
        'ranks' => $ranks,
        'roles' => $roles,
        'currencies' => $currencies,
        'technologies' => $technologies,
        'engLangLvls' => $engLangLvls,
        'statuses' => $statuses,
        'departments' => $departments,
    ]) ?>

</div>
