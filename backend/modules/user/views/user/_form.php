<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\rating\StarRating;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use app\modules\user\BillingCardAssets;
use app\modules\payments\models\Currency;


/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $form yii\widgets\ActiveForm */
/* @var $ranks[] app\modules\user\models\Rank */
/* @var $roles[] use yii\rbac\Role; */
/* @var $currencies[] app\modules\currency\models\Currency */
/* @var $technologies[] app\modules\technology\models\Technology */
/* @var $action string */
/* @var $statuses[] string */

BillingCardAssets::register($this);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <div class="row">
        <!-- left section -->
        <div class="col-xs-12 col-sm-6">

            <?= $form->field($profileModel, 'first_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'middle_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userModel, 'username')->textInput(['maxlength' => true]) ?>

            <?php
            $passwordOptions = ['maxlength' => true, 'value' => ''];
            if ($userModel->isNewRecord) {
                $passwordOptions['required'] = 'required';
            }
            ?>
            <?= $form->field($userModel, 'password')->passwordInput($passwordOptions) ?>

        </div>

        <!-- right section -->
        <div class="col-xs-12 col-sm-6">

            <?= $form->field($userModel, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'skype')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'icq')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'rating')->widget(StarRating::className(), [
              'name' => 'rating',
              'pluginOptions' => [
                'size' => 'sm',
                'stars' => 10,
                'min' => 0,
                'max' => 10,
                'step' => 0.5,
                'defaultCaption' => '{rating} stars',
                'starCaptions' => [
                    10 => 'Very Good',
                ],
                'starCaptionClasses' => [
                    '0' => 'text-danger',
                    '0.5' => 'text-danger',
                    '1' => 'text-danger',
                    '1.5' => 'text-danger',
                    '2' => 'text-warning',
                    '2.5' => 'text-warning',
                    '3' => 'text-warning',
                    '3.5' => 'text-warning',
                    '4' => 'text-info',
                    '4.5' => 'text-info',
                    '5' => 'text-info',
                    '5.5' => 'text-info',
                    '6' => 'text-primary',
                    '6.5' => 'text-primary',
                    '7' => 'text-primary',
                    '7.5' => 'text-primary',
                    '8' => 'text-success',
                    '8.5' => 'text-success',
                    '9' => 'text-success',
                    '9.5' => 'text-success',
                    '10' => 'text-success'
                ]
              ]
            ]); ?>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-3">
            <?= $form->field($userModel, 'status')->dropDownList($statuses) ?>
        </div>

        <div class="col-xs-3">
            <?= $form->field($profileModel, 'department_id')
                ->radioList(ArrayHelper::map($departments, 'id', 'name')); ?>
        </div>

        <div class="col-xs-3">
            <?= $form->field($profileModel, 'email_home')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-xs-3">
            <?= $form->field($profileModel, 'birthday')->widget(DatePicker::className(), [
                'name' => 'birthday',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'value' => '',
                'pluginOptions' => [
                    'autoClose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
    </div>

    <hr/>
    <div class="row">

        <div class="col-xs-12 col-sm-3">
            <?= $form->field($userModel, 'rolesAsArray')->widget(Select2::className(), [
              'name' => 'rolesAsArray',
              'data' => ArrayHelper::map($roles, 'name', 'name'),
              'size' => Select2::MEDIUM,
              'options' => ['placeholder' => Yii::t('user', '--select role(s)--'), 'multiple' => true],
              'pluginOptions' => ['allowClear' => true],
            ]);?>
        </div>

        <div class="col-xs-12 col-sm-3">
            <?= $form->field($profileModel, 'rank_id')
                ->radioList(ArrayHelper::map($ranks, 'id', 'name'), ['prompt'=>Yii::t('user', 'Select rank')]); ?>
        </div>

        <div class="col-xs-12 col-sm-3">
            <?= $form->field($userModel, 'technologiesIds')->widget(Select2::className(), [
              'name' => 'technologiesIds',
              'data' => ArrayHelper::map($technologies, 'id', 'name'),
              'size' => Select2::MEDIUM,
              'options' => ['placeholder' => Yii::t('user', '--select technologie(s)--'), 'multiple' => true],
              'pluginOptions' => ['allowClear' => true],
            ]);?>
        </div>

        <div class="col-xs-12 col-sm-3">
                <?= $form->field($profileModel, 'englishLanguageLevel_id')
                    ->radioList(ArrayHelper::map($engLangLvls, 'id', 'name'), ['prompt'=>Yii::t('user', 'Select english language level')]) ?>
        </div>
    </div>

    <hr/>
    <div class="row">

        <div class="col-xs-12 col-sm-6">

            <div class="col-xs-7">
                <?= $form->field($profileModel, 'monthly_rate')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-xs-5">
                <?= $form->field($profileModel, 'monthly_rate_currency_id')
                    ->radioList(ArrayHelper::map($currencies, 'id', 'name'), ['prompt'=>Yii::t('user', 'Select currency')]) ?>
            </div>

        </div>

        <div class="col-xs-12 col-sm-6">

            <div class="col-xs-7">
                <?= $form->field($profileModel, 'hourly_rate')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-xs-5">
                <?= $form->field($profileModel, 'hourly_rate_currency_id')
                    ->radioList(ArrayHelper::map($currencies, 'id', 'name'), ['prompt'=>Yii::t('user', 'Select currency')]) ?>
            </div>

        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="col-xs-7">
                <div id="billing-cards">
                    <ul class="list-group">
                        <?php if (!empty($billingCards)): ?>
                            <span><?= Yii::t('user', 'List of cards:') ?></span>
                            <?php foreach($billingCards as $item): ?>
                                <li class="list-group-item">
                                    <?= Html::a('<span class="glyphicon glyphicon-remove float-right"></span>', ['/user/billing-card/delete', 'id' => $item['id']],
                                        [
                                            'class' => 'deleteCard',
                                        ])
                                    ?>
                                    <?= $item['currencyAsString'] . ': ' . $item['number'] ?>
                                </li>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <span><?= Yii::t('user', 'There is no cards added') ?></span>
                        <?php endif; ?>
                    </ul>
                </div>
                <?= Html::button(Yii::t('user', 'Add Billing Card'), ['value' => Url::to(['billing-card/create']), 'class' => 'btn btn-success', 'id'=>'modalButtonAddBillingCard']) ?>
                <?php
                Modal::begin([
                    'id' => 'modalAddBillingCard',
                ]);
                echo "<div id='modalContentAddBillingCard'></div>";
                Modal::end();
                ?>
            </div>
        </div>
    </div>
    <hr/>

    <?php if (!empty($profileModel->profile_image)) {
            $prev = Html::img(Yii::$app->urlManager->baseUrl . '/uploads/avatars/' . $profileModel->profile_image, [
              'class' => 'file-preview-image',
              'alt' => Yii::t('user', 'Avatar for ') . $profileModel->first_name . '.',
            ]);
            $url = Url::to(['/user/deleteavatar', 'id' => $profileModel->id]);
            $initialPreviewConfig[] = ['url' => $url];
    } else {
            $prev = null;
            $initialPreviewConfig = [];
    } ?>
    <?= $form->field($profileModel, 'profileImage')->widget(FileInput::className(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showUpload' => false,
                'showRemove' => false,
                'initialPreview' => [
                    $prev,
                ],
              'initialPreviewConfig' => $initialPreviewConfig,
            ],
            'pluginEvents' => [
              'filepredelete' => "function(event, key) {
                  return (!confirm('Are you sure you want to delete user avatar?'));
              }",
              'filedeleteerror' => "function(event, data) {
                  $('div.field-profile-profileimage').find('div.file-preview-thumbnails').remove();
              }",
            ],
    ]); ?>
    <hr/>

    <div class="form-group text-center">
        <?= Html::submitButton($userModel->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $userModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
