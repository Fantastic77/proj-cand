<?php 
use yii\widgets\DetailView;
use kartik\rating\StarRating;
?>

<div class="supervisor-default-index">
	 <!-- class="text-center" -->
	<h1> <?=Yii::t('user', 'Hello')?>, <span class="green"><?= $profile->first_name . ' ' . $profile->middle_name; ?></span>!</h1>
	<?php 
	if(Yii::$app->user->can('user.hasRating'))
	{
		if (isset($profile->rank->name)) { echo '<h3>' .Yii::t('user', 'WelcomePageRattingMsg1'). ' ' . strtolower($profile->rank->name) . ' ' . Yii::t('user', 'WelcomePageRattingMsg2') . ' — ' . '</h3>' .
		 StarRating::widget([
			'name' => 'rating',
			'value' => $profile->rating,
			'pluginOptions' => [
				'size' => 'xs',
				'max' => 10,
				'stars' => 10,
				'readonly' => true,
				'showClear' => false,
				'showCaption' => true,
				'defaultCaption' => '{rating} / 10',
				'starCaptions' => [
					1 => '1 / 10',
					2 => '2 / 10',
					3 => '3 / 10',
					4 => '4 / 10',
					5 => '5 / 10',
					6 => '6 / 10',
					7 => '7 / 10',
					8 => '8 / 10',
					9 => '9 / 10',
					10 => '10 / 10',
				],
				'starCaptionClasses' => [
					'0' => 'text-danger',
					'0.5' => 'text-danger',
					'1' => 'text-danger',
					'1.5' => 'text-danger',
					'2' => 'text-warning',
					'2.5' => 'text-warning',
					'3' => 'text-warning',
					'3.5' => 'text-warning',
					'4' => 'text-info',
					'4.5' => 'text-info',
					'5' => 'text-info',
					'5.5' => 'text-info',
					'6' => 'text-primary',
					'6.5' => 'text-primary',
					'7' => 'text-primary',
					'7.5' => 'text-primary',
					'8' => 'text-success',
					'8.5' => 'text-success',
					'9' => 'text-success',
					'9.5' => 'text-success',
					'10' => 'text-success'
				]
			],
		]);
		} else { echo '<h3>' .Yii::t('user', 'WelcomePageRattingMsg1'). ': ' . '<span class="not-set">('.Yii::t('user', 'not setted').')</span>' . '</h3>'; }
	}
	?>

	<h3> <?=Yii::t('user', 'Your english language level') ?>:
	<?php 
	if($profile->englishLanguageLevel === null){
		echo '<span class="not-set">('.Yii::t('user', 'not setted').')</span>';
	}
	else{
		echo strtolower($profile->englishLanguageLevel->name) . '.'; 
	}
	?>
	</h3>

	<h3> <?=Yii::t('user', 'Status') ?>: <?= $user->statusAsString; ?>.</h3>
	<h3> <?=Yii::t('user', 'payable balance') ?>: <?= 'balance' . ' ' . 'UAH' ?>.</h3>

</div>