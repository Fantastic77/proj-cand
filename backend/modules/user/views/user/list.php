<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use app\modules\user\UserAssets;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\searchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $actionButtonsTemplate String */
/* @var $additionalButtonsTemplate String */
/* @var $ranks[] app\modules\user\models\Rank */
/* @var $otherUsers[] app\modules\user\models\User */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;

UserAssets::register($this);

?>
<div class="user-index">

    <?= $this->render('@app/modules/dialog/views/dialog/_dialogCreateModal', [
        'otherUsers' => $otherUsers
    ]) ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (Yii::$app->user->can('user.create')) { ?>
        <p>
            <?= Html::a(Yii::t('user', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <?php Pjax::begin();?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'fullName',
                [
                    'attribute' => 'rolesAsString',
                    'filter' => ArrayHelper::map($roles, 'name', 'name'),
                ],
                [
                    'attribute' => 'rank',
                    'filter' => ArrayHelper::map($ranks, 'id', 'name'),
                ],

                [
                    'label' => Yii::t('user', 'Department'),
                    //'attribute' => 'profile.department.name',
                    'attribute' => 'department',
                    'filter' => ArrayHelper::map($departments, 'id', 'name'),
                ],

                'monthlyRateAsString',
                'rating',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $actionButtonsTemplate,
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $additionalButtonsTemplate,
                    'buttons' => [
                        'message' => function ($url, $model) {
                            return '<a href="javascript:void(0)"><span class="glyphicon glyphicon-envelope send-message" data-target="#create-dialog-modal" data-toggle="modal" data-user-id="' . $model->id .'"></span></a>';
                        },
                        'complain' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-exclamation-sign"></span>', "/complaint/create/$model->id",
                                [
                                    'title' => Yii::t('user', 'Complain'),
                                ]);
                        },
                    ],
                ],
            ]
        ]); ?>
    <?php Pjax::end();?>
</div>
