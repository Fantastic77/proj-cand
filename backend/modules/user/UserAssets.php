<?php

namespace app\modules\user;

use yii\web\AssetBundle;

class UserAssets extends AssetBundle
{
  public $sourcePath = '@app/modules/user/assets/user';
//    public $baseUrl;
  public $css = [
    'css/user.css'
  ];

  public $js = [
    'js/user.js',
  ];

  public $depends = [
    'yii\web\JqueryAsset',
    'yii\web\YiiAsset',
    'yii\widgets\ActiveFormAsset',
  ];
}