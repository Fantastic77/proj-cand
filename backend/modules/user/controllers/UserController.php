<?php

namespace app\modules\user\controllers;

use app\modules\payments\models\Currency;
use app\modules\project\models\form\UploadForm;
use app\modules\technology\models\Technology;
use app\modules\technology\models\UserTechnology;
use app\modules\user\models\Rank;
use app\modules\englishLanguageLevels\models\EnglishLanguageLevel;
use app\modules\user\models\search\BillingCardSearch;
use app\modules\user\models\BillingCard;
use app\modules\department\models\Department;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\rbac\Role;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\user\models\search\searchUser;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['welcome'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['welcome'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionWelcome()
    {
        $user = Yii::$app->user->getIdentity();
        $profile = $user->profile;
        $roles = $user->roles;
        $userModel = $this->findModel($profile->id);
        
        return $this->render('welcome', array(
            'user' => $user,
            'profile' => $profile,
            'roles' => $roles,
            'userModel' => $userModel,
        ));
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionList()
    {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $searchModel = new searchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $actionButtonsTemplate = Yii::$app->actionButtonsHelper->getActionButtonsTemplate('user');
        $additionalButtonsTemplate = Yii::$app->actionButtonsHelper->getActionButtonsTemplate('user', false, [
            'user.sendMessage' => 'message',
            'user.canComplain' => 'complain',
        ]);

        $ranks = Rank::find()->all();
        $roles = Yii::$app->authManager->roles;
        $departments = Department::find()->all();

        $otherUsers = User::find()
            ->with('profile')
            ->where(['!=', 'id', Yii::$app->user->id])
            ->all();

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionButtonsTemplate' => $actionButtonsTemplate,
            'additionalButtonsTemplate' => $additionalButtonsTemplate,
            'ranks' => $ranks,
            'roles' => $roles,
            'departments' => $departments,
            'otherUsers' => $otherUsers
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('user.read') && !Yii::$app->user->can('user.profileView')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        return $this->render('view', [
            'userModel' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('user.create')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $userModel = new User();
        $profileModel = new Profile();
        $ranks = Rank::find()->all();
        $roles = Yii::$app->authManager->roles;
        $statuses = User::statusDropdown();
        $currencies = Currency::find()->all();
        $technologies = Technology::find()->all();
        $engLangLvls = EnglishLanguagelevel::find()->all();
        $departments = Department::find()->all();

        if ($userModel->load(Yii::$app->request->post()) && $profileModel->load(Yii::$app->request->post())) {
            $isValid = $userModel->validate();
            $isValid = $profileModel->validate() && $isValid;
            if ($isValid) {
                // get billing info and save to JSON
//                $billing = ['USD' => $profileModel->billingUSD, 'UAH' => $profileModel->billingUAH];
//                $profileModel->billing_info = json_encode($billing, JSON_FORCE_OBJECT);
//                $profileModel->status = Profile::STATUS_PROFILE_FREE;

                // save profile and user
                $profileModel->save(false);
                $userModel->profile_id = $profileModel->id;
                $userModel->password = Yii::$app->security->generatePasswordHash($userModel->password);

                $userModel->created_at = date('Y-m-d h:i:s');
                $userModel->updated_at = date('Y-m-d h:i:s');
                $userModel->save(false);

                // save user roles
                if (!empty(Yii::$app->request->post('User')['rolesAsArray'])) {
                    foreach(Yii::$app->request->post('User')['rolesAsArray'] as $key => $roleName) {
                        $role = new Role();
                        $role->name = $roleName;
                        Yii::$app->authManager->assign($role, $userModel->id);
                    }
                }

                // save user technologies
                if (!empty(Yii::$app->request->post('User')['technologiesIds'])) {
                    foreach(Yii::$app->request->post('User')['technologiesIds'] as $technologyId) {
                        $userTechnology = new UserTechnology($userModel->id, $technologyId);
                        $userTechnology->save();
                    }
                }

                //save profile image
                $profileModel->profileImage = UploadedFile::getInstance($profileModel, 'profileImage');
                if($profileModel->profileImage && $profileModel->profileImage->tempName) {
                    if($profileModel->validate('profileImage')) {
                        $dir = 'uploads/avatars/';
                        if (!file_exists($dir)) {
                            mkdir($dir, 0777, true);
                        }
                        $fileName = time() . '.' . $profileModel->profileImage->extension;
                        $profileModel->profileImage->saveAs($dir . $fileName);
                        $profileModel->profile_image = $fileName;
                        $profileModel->profileImage = null;
                        $profileModel->save();
                    }
                }

                //save billing cards
                if (isset($_POST['card'])) {
                    foreach ($_POST['card'] as $card) {
                        $cardInfo = explode(";", $card);

                        $numberRow = explode("=", $cardInfo[0]);
                        $currencyIdRow = explode("=", $cardInfo[1]);

                        $number = $numberRow[1];
                        $currencyId = $currencyIdRow[1];

                        $billingCard = new BillingCard();
                        $billingCard->owner_id = $userModel->id;
                        $billingCard->number = $number;
                        $billingCard->currency_id = $currencyId;
                        $billingCard->save();
                    }
                }

                return $this->redirect(['view', 'id' => $userModel->id]);
            }
        }
        return $this->render('create', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'ranks' => $ranks,
            'roles' => $roles,
            'currencies' => $currencies,
            'technologies' => $technologies,
            'engLangLvls' => $engLangLvls,
            'statuses' => $statuses,
            'departments' => $departments,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('user.update') && !Yii::$app->user->can('user.profileView')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $userModel = $this->findModel($id);
        $profileModel = $userModel->profile;
        $ranks = Rank::find()->all();
        $roles = Yii::$app->authManager->roles;
        $statuses = User::statusDropdown();
        $currencies = Currency::find()->all();
        $technologies = Technology::find()->all();
        $engLangLvls = EnglishLanguagelevel::find()->all();
        $billingCardsSearchModel = new BillingCardSearch;
        $billingCards = $billingCardsSearchModel->getEmployeeBillingCards($userModel->id);
        $departments = Department::find()->all();

//        if (!empty($profileModel->billing_info)) {
//            $profileModel->billingUSD = json_decode($profileModel->billing_info)->USD;
//            $profileModel->billingUAH = json_decode($profileModel->billing_info)->UAH;
//        }

        if ($userModel->load(Yii::$app->request->post()) && $profileModel->load(Yii::$app->request->post())) {
            $isValid = $userModel->validate();
            $isValid = $profileModel->validate() && $isValid;

            if ($isValid) {
                // get billing info and save to JSON
//                $billing = ['USD' => $profileModel->billingUSD, 'UAH' => $profileModel->billingUAH];
//                $profileModel->billing_info = json_encode($billing, JSON_FORCE_OBJECT);

                // save profile and user
                $profileModel->save(false);
                if (Yii::$app->user->can('user.create')) {
                    if (!empty($userModel->password)) {
                        $userModel->password = Yii::$app->security->generatePasswordHash($userModel->password);
                    } else {
                        $userModel->password = $userModel->oldAttributes['password'];
                    }
                }
                $userModel->updated_at = date('Y-m-d h:i:s');
                $userModel->save(false);

                // save user roles
                foreach($roles as $role){
                    // remove all roles from user
                    if (Yii::$app->user->can('user.create')) Yii::$app->authManager->revoke($role, $userModel->id);
                }
                if (!empty(Yii::$app->request->post('User')['rolesAsArray'])) {
                    foreach(Yii::$app->request->post('User')['rolesAsArray'] as $key => $roleName) {
                        $role = new Role();
                        $role->name = $roleName;
                        Yii::$app->authManager->assign($role, $userModel->id);
                    }
                }

                // save user technologies
                UserTechnology::deleteAll(['user_id' => $userModel->id]);
                if (!empty(Yii::$app->request->post('User')['technologiesIds'])) {
                    foreach(Yii::$app->request->post('User')['technologiesIds'] as $technologyId) {
                        $userTechnology = new UserTechnology($userModel->id, $technologyId);
                        $userTechnology->save();
                    }
                }

                $profileModel->profileImage = UploadedFile::getInstance($profileModel, 'profileImage');
                if($profileModel->profileImage && $profileModel->profileImage->tempName) {
                    if($profileModel->validate('profileImage')) {
                        $dir = 'uploads/avatars/';
                        if (!file_exists($dir)) {
                            mkdir($dir, 0777, true);
                        }
                        $fileName = time() . '.' . $profileModel->profileImage->extension;
                        if (!empty($profileModel->profile_image)) {
                            @unlink('uploads/avatars/' . $profileModel->profile_image);
                        }
                        $profileModel->profileImage->saveAs($dir . $fileName);
                        $profileModel->profile_image = $fileName;
                        $profileModel->profileImage = null;
                        $profileModel->save();
                    }
                }

                //save billing cards
                if (isset($_POST['card'])) {
                    foreach ($_POST['card'] as $card) {
                        $cardInfo = explode(";", $card);

                        $numberRow = explode("=", $cardInfo[0]);
                        $currencyIdRow = explode("=", $cardInfo[1]);

                        $number = $numberRow[1];
                        $currencyId = $currencyIdRow[1];

                        $billingCard = new BillingCard();
                        $billingCard->owner_id = $userModel->id;
                        $billingCard->number = $number;
                        $billingCard->currency_id = $currencyId;
                        $billingCard->save();
                    }
                }

                return $this->redirect(['view', 'id' => $userModel->id]);
            }
        }
        return $this->render('update', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'ranks' => $ranks,
            'roles' => $roles,
            'currencies' => $currencies,
            'technologies' => $technologies,
            'engLangLvls' => $engLangLvls,
            'statuses' => $statuses,
            'departments' => $departments,
            'billingCards' => $billingCards,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('user.delete')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $model = $this->findModel($id);
        if(!empty($model->profile_image)) {
            @unlink('uploads/avatars/' . $model->profile_image);
        }
        $model->delete();
        return $this->redirect(['list']);
    }

    public function actionDeleteavatar($id)
    {
        $model = $this->findModel($id);
        if (@unlink('uploads/avatars/' . $model->profile->profile_image)) {
            $model->profile->profile_image = null;
            $model->profile->save(false);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['error' => Yii::t('user', 'Profile image deleted')];
        }
    }

    public function actionStatus($id) {
        $model = $this->findModel($id);
        $profile = $model->profile;
        $profile->scenario = Profile::SCENARIO_STATUS;
        $post = Yii::$app->request->post();

        if ($profile->load($post) && $profile->validate()) {
            $profile->save(false);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $profile->getStatusAsString($profile->status);
        } else {
            return $this->renderAjax('status', [
              'profile' => $profile,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
