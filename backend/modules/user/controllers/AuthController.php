<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\user\models\forms\LoginForm;

class AuthController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $loginDuration = $this->module->loginDuration;
            if ($model->validate() && $model->login($loginDuration)){
                return $this->redirect('/welcome');
            }
        }
        $this->layout = '@app/views/layouts/main';
        return $this->render('login', ['model' => $model]);
    }

    public function actionLogout()
    {
        $user = Yii::$app->user->getIdentity();
        $user->disableAccessToken();
        Yii::$app->user->deleteUserAfterLogout;
        Yii::$app->user->logout();
        return $this->redirect('/');
    }
}
