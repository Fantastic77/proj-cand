<?php

namespace app\modules\user\controllers;

use app\modules\payments\models\Currency;
use app\modules\project\models\form\UploadForm;
use app\modules\technology\models\Technology;
use app\modules\technology\models\UserTechnology;
use app\modules\user\models\Rank;
use app\modules\englishLanguageLevels\models\EnglishLanguageLevel;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\rbac\Role;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\user\models\search\searchUser;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use app\modules\department\models\Department;

/**
 * UserController implements the CRUD actions for User model.
 */
class EmployeeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['list'],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => ['list'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['list'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionList()
    {
        if (!Yii::$app->user->can('user.employeesList')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $searchModel = new searchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $ranks = Rank::find()->all();
        $departments = Department::find()->all();
        $roles = Yii::$app->authManager->roles;
        $profileStatuses = Profile::statusDropdown();
        $actionButtonsTemplate = Yii::$app->actionButtonsHelper->getActionButtonsTemplate('user', false, [
            'user.sendMessage' => 'message',
            'user.canComplain' => 'complain',
        ]);

        $otherUsers = User::find()
            ->with('profile')
            ->where(['!=', 'id', Yii::$app->user->id])
            ->all();

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ranks' => $ranks,
            'roles' => $roles,
            'profileStatuses' => $profileStatuses,
            'actionButtonsTemplate' => $actionButtonsTemplate,
            'departments' => $departments,
            'otherUsers' => $otherUsers
        ]);
    }

    public function actionInfo($id)
    {
        $tempUser = new User();
        $labelsUser = $tempUser->attributeLabels();
        $tempProfile = new Profile();
        $labelsProfile = $tempProfile->attributeLabels();
        $user = $this->findModel($id);
        $result = [];
        $result['online'] = [
            'label' => Yii::t('app', 'Online'),
            'value' => $user->userOnline,
        ];
        $result['profile_image'] = [
          'label' => $labelsProfile['profile_image'],
          'value' => isset($user->profile->profile_image) ? Yii::$app->user->avatarsDir . $user->profile->profile_image : Profile::$defaultImage,
        ];
        $result['first_name'] = [
          'label' => $labelsProfile['first_name'],
          'value' => $user->profile->first_name,
        ];
        $result['last_name'] = [
          'label' => $labelsProfile['last_name'],
          'value' => $user->profile->last_name,
        ];
        $result['middle_name'] = [
          'label' => $labelsProfile['middle_name'],
          'value' => $user->profile->middle_name,
        ];
        $result['rank'] = [
          'label' => $labelsUser['rank'],
          'value' => $user->rank,
        ];
        $result['roles'] = [
          'label' => $labelsUser['rolesAsString'],
          'value' => $user->rolesAsString,
        ];
        $result['profile_status'] = [
          'label' => $labelsProfile['status'],
          'value' => $user->profile->getStatusAsString($user->profile->status),
        ];
        $result['profile_status_description'] = [
          'label' => $labelsProfile['status_description'],
          'value' => $user->profile->status_description,
        ];
        $result['department'] = [
          'label' => $labelsProfile['department_id'],
          'value' => $user->getDepartment(),
        ];
        $result['rating'] = [
          'label' => $labelsProfile['rating'],
          'value' => $user->profile->rating,
        ];
        $result['skype'] = [
          'label' => $labelsProfile['skype'],
          'value' => $user->profile->skype,
        ];
        $result['icq'] = [
          'label' => $labelsProfile['icq'],
          'value' => $user->profile->icq,
        ];
        $result['phone'] = [
          'label' => $labelsProfile['phone'],
          'value' => $user->profile->phone,
        ];
        $result['email'] = [
            'label' => $labelsUser['email'],
            'value' => Yii::$app->formatter->asEmail($user->email),
        ];
        $result['english_level'] = [
            'label' => $labelsProfile['englishLanguageLevel_id'],
            'value' => $user->profile->englishLanguageLevel['name'],
        ];
        $result['message'] = [
            'label' => Yii::t('user', 'Message'),
            'value' => '/user/message/',
        ];
        $updatedDate = $user->updated_at;
        if (!empty($updatedDate)) {
            $updatedDate = Yii::$app->formatter->asDatetime('now', $user->updated_at);
        } else {
            $updatedDate = Yii::$app->formatter->asDatetime($user->updated_at);
        }
        $result['updated'] = [
            'label' => Yii::t('user', 'Updated date'),
            'value' => $updatedDate,
        ];

        $result['email_home'] = [
            'label' => $labelsProfile['email_home'],
            'value' => Yii::$app->formatter->asEmail($user->profile->email_home),
        ];
        $result['birthday'] = [
            'label' => $labelsProfile['birthday'],
            'value' => $user->profile->birthday,
        ];

        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return (isset($result)) ? $result : [];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
