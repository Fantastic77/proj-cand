<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\BillingCard;
use app\modules\user\models\search\BillingCardSearch as BillingCardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\payments\models\Currency;
use yii\web\Response;

/**
 * BillingCardController implements the CRUD actions for BillingCard model.
 */
class BillingCardController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['post'],
                    'update' => ['post'],
                    // ����� ��������� �� ���������� ������ �� ��� ������ ����� ������
                    //'create' => ['get'],
                    //'delete' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all BillingCard models.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new BillingCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BillingCard model.
     * @param integer $id
     * @return mixed
     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionShow(){
        $array = $_POST['newUserBillingCards'];
        if(is_array($array)) {
            foreach ($array as $item) {
                $number = $item['number'];
                echo $number;
            }
        }
        die();
    }

    /**
     * Creates a new BillingCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //if (!Yii::$app->user->can('user.create')) {
        //    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        //}

        $model = new BillingCard();
        $currencies = Currency::find()->all();

        //if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->validate()) {
                return [
                    'save' => true,
                    'billingCardModel' => $model,
                    'currencyAsString' => $model->getCurrencyAsString($model->currency_id),
                    'text' => Yii::t('user', 'List of cards:')
                ];
            }
            else{
                return [
                    'save' => false
                ];
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'currencies' => $currencies,
                ]);
            }
        }
    }


    /**
     * Updates an existing BillingCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $currencies = Currency::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'currencies' => $currencies,
            ]);
        }
    }

    /**
     * Deletes an existing BillingCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax) {
            if($id != null) {
                $this->findModel($id)->delete();
                return ['save' => true, 'text' => Yii::t('user', 'There is no cards added')];
            }
            else {
                return ['save' => true, 'text' => Yii::t('user', 'There is no cards added')];
            }
        }

        return ['save' => false];

    }

    /**
     * Finds the BillingCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BillingCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BillingCard::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
