<?php

use yii\db\Migration;


class m180323_083057_create_table_candidate extends Migration
{
    protected $tn_candidate = '{{%candidate}}';
    protected $tn_framework = '{{%framework}}';
    protected $tn_candidate_framework = '{{%candidate_framework}}';

    public function safeUp()
    {
        $this->createTable($this->tn_candidate, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'birthday' => $this->date(),
            'experience' => $this->integer(),
            'comment' => $this->string(),
            'created_at' => $this->dateTime(),
            'Yii1' => $this->boolean(),
            'Yii2' => $this->boolean(),
            'Laravel' => $this->boolean(),
            'Symphony' => $this->boolean(),
            'Zend' => $this->boolean(),
        ]);

        $this->createTable($this->tn_framework, [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        $this->createTable($this->tn_candidate_framework, [
            'candidate_id' => $this->integer(),
            'framework_id' => $this->integer(),
            'PRIMARY KEY (candidate_id, framework_id)'
        ]);

        $this->addForeignKey('fk_candidate_framework_candidate_id', $this->tn_candidate_framework, 'candidate_id', $this->tn_candidate, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_candidate_framework_framework_id', $this->tn_candidate_framework, 'framework_id', $this->tn_framework, 'id', 'CASCADE', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_candidate_framework_candidate_id', $this->tn_candidate_framework);
        $this->dropForeignKey('fk_candidate_framework_framework_id', $this->tn_candidate_framework);

        $this->dropTable($this->tn_candidate_framework);
        $this->dropTable($this->tn_framework);
        $this->dropTable($this->tn_candidate);
    }
}
