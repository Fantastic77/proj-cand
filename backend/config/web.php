<?php

$params = require(__DIR__ . '/params.php');
$config = [
    'id' => 'basic',
    'language' => 'uk-UA',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@mdm/admin' => '@app/modules/rbac',
    ],
    'timeZone' => 'UTC+2',
    'as access' => [
        'class' => 'app\modules\rbac\components\AccessControl',
        'allowActions' => [
            'candidate/*',
        ]
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation'=>false,
            'cookieValidationKey' => 'Bd8hiAJQf02x',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
            // ...
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // technology
                'technology/<action:[-\w]+>' => 'technology/technology/<action>',
                'technology/<action:[-\w]+>/<id:\w+>' => 'technology/technology/<action>',

                // candidate
                'candidate/<action:[-\w]+>' => 'candidate/candidate/<action>',
                'candidate/<action:[-\w]+>/<id:\w+>' => 'candidate/candidate/<action>',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\modules\user\components\User',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'username',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls',
            ],
            'useFileTransport' => true,
            'messageConfig' => [
                'from' => ['admin@supervisor.com' => 'Admin'],
                'charset' => 'UTF-8',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mainMenu' => [
            'class' => 'app\components\MainMenu',
        ],
        'actionButtonsHelper' => [
            'class' => 'app\components\ActionButtonsHelper',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'forceCopy' => true,
        ],
    ],
    'modules' => [
        'technology' => [
            'class' => 'app\modules\technology\Module',
        ],
        'candidate' => [
            'class' => 'app\modules\candidate\Module',
        ],
    ],
    'params' => $params,
];

$config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/db.php'));

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*.*.*.*'],
    ]; 
}

return $config;
