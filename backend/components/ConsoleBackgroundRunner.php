<?php

namespace app\components;

use Yii;
use yii\base\Component;

// run console commands in background

class ConsoleBackgroundRunner extends Component
{
    private $_yiiFilePath = '@app/yii';

    /**
     * Running console command on background
     *
     * @param string $cmd Argument that will be passed to console application
     * @return boolean
     */
    public function run($cmd)
    {
        ignore_user_abort(true);
        $cmd = 'php ' . Yii::getAlias($this->_yiiFilePath) . ' ' . $cmd;
        if ($this->isWindows() === true) {
            popen('start /b ' . $cmd, 'r');
        } else {
            popen($cmd . ' > /dev/null &', 'r');
        }
        return true;
    }

    /**
     * Check operating system
     *
     * @return boolean true if it's Windows OS
     */
    protected function isWindows()
    {
        if (PHP_OS == 'WINNT' || PHP_OS == 'WIN32') {
            return true;
        } else {
            return false;
        }
    }

}