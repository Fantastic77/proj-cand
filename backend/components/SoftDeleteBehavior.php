<?php

namespace app\components;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

class SoftDeleteBehavior extends TimestampBehavior
{
    /**
     * @var string SoftDelete attribute
     */
    public $deletedAtAttribute = 'deleted_at';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [ActiveRecord::EVENT_BEFORE_DELETE => 'softDelete'];
    }

    /**
     * Event handler
     *
     * @param $event
     */
    public function softDelete($event)
    {
        $this->remove();
        $event->isValid = false;
    }

    /**
     * Mark model as deleted
     */
    public function remove()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;

        $timestamp = $this->getValue(null);
        $attribute = $this->deletedAtAttribute;

        $model->$attribute = $timestamp;

        $model->save(false, [$attribute]);
    }

    /**
     * Restore model
     */
    public function restore()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;

        $attribute = $this->deletedAtAttribute;

        $model->$attribute = null;

        $model->save(false, [$attribute]);
    }

    /**
     * Delete model from DB
     *
     * @throws \yii\db\StaleObjectException
     */
    public function forceDelete()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;

        $this->detach();
        $model->delete();
    }

    /**
     * Check is the model deleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $attribute = $this->deletedAtAttribute;

        return $model->$attribute === null;
    }
}