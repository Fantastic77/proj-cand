<?php

namespace app\assets;

use kartik\base\AssetBundle;

class ScrollBarAsset extends AssetBundle
{
    public $sourcePath = '@bower/perfect-scrollbar';
    public $css = [
        'css/perfect-scrollbar.min.css'
    ];
    public $js = [
        'js/min/perfect-scrollbar.jquery.min.js',
        'js/min/perfect-scrollbar.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}